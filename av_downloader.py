#! /usr/bin/env python3
# -*- coding: utf-8; -*-

import re

ARCHIVO_ENLACES="links"
REGEX_LINK="(https?:\/\/)(aulavirtual.um.es\/portal\/site\/)(\d{4}_G_\d{4}_N_N)(\/.*)"
REGEX_ASIGNATURA="(\d{4}_G_\d{4}_N_N)"

p=re.compile(REGEX_ASIGNATURA)

def main():
	# Para cada enlace de la lista
	with open(ARCHIVO_ENLACES) as enlaces:
		for enlace in enlaces:
			print("Obteniendo enlace...")
			match = p.findall(enlace)
			print(match)
	
	return 0

if __name__ == '__main__':
	main()